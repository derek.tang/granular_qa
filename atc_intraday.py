import os
import numpy as np
import pandas as pd
import psycopg2
from contextlib import closing
import seaborn as sns
import plotly.graph_objects as go
import plotly.express as px
import plotly.io as pio
from plotly.subplots import make_subplots

pio.renderers.default = 'browser'

### INPUTS: Edit parameters here.
base = "PMF CAI 23 OCT MESSY TRANSITION FINAL-FYR"
scenario = "PMF CAI 24 JAN MT dev 16-FYR"
years = [2025, 2026]
y_granularity = 'season'  # 'month' or 'season'


def sql(sqlquery, params=None, dbname='production'):
    """ Takes a SQL query, opens a connection, queries the data, puts them into an pandas dataframe
                          and closes the connection """

    user = os.environ.get('dwh_user_name')
    password = os.environ.get('dwh_password')

    host = "warehouse.chgkrthnr8sn.eu-west-2.redshift.amazonaws.com"

    user = user + "_" + dbname
    port = "5439"
    connect_string = f"host={host} dbname={dbname} user={user} password={password} port={port}"
    with closing(psycopg2.connect(connect_string)) as conn:
        out_df = pd.read_sql_query(sqlquery, conn, params=params)
    print('query done')
    return out_df


def time_labels(df):
    df['date'] = pd.to_datetime(df['date'])
    df.set_index('date', inplace=True)
    df['year'] = df.index.year
    df['month'] = df.index.month
    df['day'] = df.index.day
    df['hour'] = df.index.hour
    df['season'] = pd.cut(df.month, bins=[0, 2, 5, 8, 11, 12], labels=['Winter', 'Spring', 'Summer', 'Fall', 'Winter'],
                          ordered=False)
    return df


def atc(scenario, alt_scenario):
    atc_query = f"""
    Select t.date, r.region, s.scenario, hhr.wholesaleprice as ATC 
    from halfhourlyregion hhr
    inner join time t on t.id = hhr.timeid
    inner join scenarios s on s.id = hhr.scenarioid
    inner join regions r on r.id = hhr.regionid
    where s.scenario in ('{scenario}','{base}') and r.region in ('NP15','SP15','ZP26','CAISO')
    order by date, region
    """

    df = sql(atc_query)
    df = time_labels(df)
    return df


def his_atc():
    his_atc = f'''
    select
      key__date_time_utc at time zone 'america/los_angeles' as date,
      key__region as region,
      price as atc
    from ad_hoc.usa_caiso_1h_dam_trading_hub_prices
    where key__category = 'LMP'
    order by date, region
    '''
    his = sql(his_atc).reset_index()
    his_filtered = time_labels(his)
    return his_filtered


def forecast(scenario, base):
    df = atc(scenario, base)
    filter_df = df[df['year'].isin(years)].groupby(['scenario', 'year', y_granularity, 'hour', 'region'])[
        'atc'].mean().reset_index()

    fig = px.line(filter_df,
                  x="hour",
                  y="atc",
                  color='region',
                  facet_row=y_granularity,
                  facet_col="year",
                  title='ATC: ' + scenario,
                  line_dash='scenario',
                  hover_data={'region': False,
                              'scenario': False,
                              y_granularity: False,
                              'year': False,
                              'hour': True,
                              'atc': True})

    fig.update_layout(legend=dict(
        orientation="h",
        yanchor="bottom",
        xanchor="auto"),
        hovermode="x unified",
        height=1700)

    fig.show()
    fig.write_html('out/forecasted_atc.html')
    return


def historical(df):
    his_filtered = df.groupby(['year', y_granularity, 'hour', 'region'])['atc'].mean().reset_index()

    fig = px.line(his_filtered,
                  x="hour",
                  y="atc",
                  color='region',
                  facet_row=y_granularity,
                  facet_col="year",
                  title='ATC: ',
                  hover_data={'region': False,
                              y_granularity: False,
                              'year': False,
                              'hour': True,
                              'atc': True})

    fig.update_layout(legend=dict(
        orientation="h",
        yanchor="bottom",
        xanchor="auto"),
        hovermode="x unified",
        height=1700)

    fig.show()
    fig.write_html('out/his_atc.html')
    return


forecast(scenario, base)
historical(his_atc())
