import numpy as np
import os
import pandas as pd
import psycopg2
from contextlib import closing
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objects as go
import plotly.express as px
import plotly.io as pio
import plotly.subplots as sp

pio.renderers.default = 'browser'

### INPUTS: Edit parameters here.
base = "PMF CAI 24 JAN CENTRAL FINAL-FYR"
scenario = "PMF CAI 24 JAN MT dev 16-FYR"
years = [2025,2030,2045,2050]
y_granularity = 'season' # 'month' or 'season'

def sql(sqlQuery, params=None, dbname='production'):
    """ Takes a SQL query, opens a connection, queries the data, puts them into an pandas dataframe
    and closes the connection """
    user = os.environ.get('dwh_user_name')
    password = os.environ.get('dwh_password')

    host = "warehouse.chgkrthnr8sn.eu-west-2.redshift.amazonaws.com"

    user = user + "_" + dbname
    port = "5439"
    connect_string = f"host={host} dbname={dbname} user={user} password={password} port={port}"
    with closing(psycopg2.connect(connect_string)) as conn:
        out_df = pd.read_sql_query(sqlQuery, conn, params=params)
    return out_df


def time_labels(df):
    df['date'] = pd.to_datetime(df['date'])
    df.set_index('date', inplace=True)
    df['year'] = df.index.year
    df['month'] = df.index.month
    df['day'] = df.index.day
    df['hour'] = df.index.hour
    df['season'] = pd.cut(df.month, bins=[0,2,5,8,11,12], labels=['Winter', 'Spring', 'Summer', 'Fall','Winter'],ordered=False)
    return df


def get_all_gen(scenario,years):
    query =f'''
    select
    extract(year from date) as year,
    extract(hour from date) as hour,
    season,
    tech,
    avg(mw) as mw
    from (
        select
        t.date,
        CASE
            when t.month in (12,1,2) then 'Winter'
            when t.month in (6,7,8) then 'Summer'
            when t.month in (9,10,11) then 'Fall'
            when t.month in (3,4,5) then 'Spring'
        END as season,
        CASE
            when technology  in ('nuk') then 'Nuclear'
            when technology in ('ccg','gascss') then 'Gas CCGT'
            when technology in ('coa','oth') then 'Other Thermal'
            when technology in ('sol','sot') then 'Utility Solar'
            when technology in ('bio','geo','ogr') then 'Other RES'
            when technology in ('won') then 'Onshore Wind'
            when technology in ('wof') then 'Offshore Wind'
            when technology in ('sto','ror') then 'Hydro'
            when technology in ('rge','occ','ocg','ico') then 'Peaking'
            when technology in ('bat') then
                CASE
                    when netproductioninmw > 0 then 'Battery Storage- Discharge'
                    when netproductioninmw < 0.01 then 'Battery Storage- Charge' end
            when technology in ('pum') then 'Pumped Storage'
            when technology in ('ips','ipc') then 'BTM Battery'
            when technology in ('sor') then 'Rooftop Solar'
            else 'Other'
        END as tech,
        sum(netproductioninmw) as mw
        from halfhourlyplant hhp
        inner join time t on t.id = hhp.timeid
        inner join scenarios s on s.id = hhp.scenarioid
        inner join plants p on p.id = hhp.plantid
        inner join regions r on r.id = p.regionid
        inner join technologies tc on tc.id = p.technologyid
        where s.scenario = '{scenario}' and r.region in ('NP15','SP15','ZP26') and t.year in {years}
        group by t.date, month, season,tech) as sub
    group by year, hour, season, tech
    order by year, hour;
    '''
    df = sql(query)
    #df = time_labels(df)
    return df

def base_demand(scenario,years):
    dem_query = f'''
    select t.date, sum(BaseDemand) as base, sum(TotalDemand) as total, sum(EVDemand) as ev
    from halfhourlyregion hhr
    inner join time t on t.id = hhr.timeid
    inner join scenarios s on s.id = hhr.scenarioid
    inner join regions r on r.id = hhr.regionid
    where s.scenario in ('{scenario}')
    and t.year in {years}
    and region in ('NP15','SP15','ZP26')
    group by t.date
    order by t.date
    '''
    df = sql(dem_query)
    df = time_labels(df)
    return df


def his_gen():
    return

def his_base_demand(scenario,years):
    dem_query = f'''
    select
      key__date_time_utc at time zone 'america/los_angeles' as date_time_CA,
      production_in_mw as demand_mw
    from ad_hoc.usa_caiso_1h_actual_demand
    where year in (2019,2020,2021)
    order by t.date
    '''
    df = sql(dem_query)
    df = time_labels(df)
    return df


def dem_all_techs_gen_area(scenario,years,y_granularity):
    seasons = ['Fall','Spring','Summer','Winter']
    dem_type = ['base','total','ev']

    df = get_all_gen(base,tuple(years))
    dem_df = base_demand(base,tuple(years))
    dem_df = pd.melt(dem_df,id_vars=['year','month','day','hour','season'],value_vars=['base','total','ev']).groupby(['year','hour','variable','season'])['value'].mean().reset_index()

    def graph():
        fig = px.area(df,
                      x="hour",
                      y="mw",
                      color='tech',
                      facet_row=y_granularity,
                      facet_col="year",
                      title= 'Res gen: ' + scenario,
                      hover_data= {'year':False,
                                   'season':False,
                                   'hour':True,
                                   'mw':True},
                      color_discrete_sequence = px.colors.qualitative.Light24)
        print('once')
        colors = ['blue', 'green', 'red']
        # Group dem_df by year and y_granularity
        for c,year in enumerate(years):
            for r,gran in enumerate(seasons):
                temp_df = dem_df[(dem_df['year'] == year) & (dem_df[y_granularity] == gran)]
                for idx, variable_value in enumerate(dem_type):
                    variable_df = temp_df[temp_df['variable'] == variable_value]
                    fig.add_trace(go.Scatter(x=variable_df['hour'], y=variable_df['value'],
                                             mode='lines',
                                             line=dict(color=colors[idx]),
                                             hoverinfo='text',
                                             text=[f'{variable_value}: {value}'
                                                   for value in variable_df['value']],
                                             showlegend=False),row=4-r,col=c+1)

        fig.update_layout(legend=dict(
            orientation="h",
            yanchor="bottom",
            xanchor="auto"),
            hovermode="x unified",
            height=1700)
        fig.write_html("out/dem_gen_area.html")
        fig.show()

    graph()
    return

def dem_all_techs_gen_bar(scenario,years,y_granularity):
    seasons = ['Fall','Spring','Summer','Winter']
    dem_type = ['base','total','ev']

    df = get_all_gen(base,tuple(years))
    dem_df = base_demand(base,tuple(years))
    dem_df = pd.melt(dem_df,id_vars=['year','month','day','hour','season'],value_vars=['base','total','ev']).groupby(['year','hour','variable','season'])['value'].mean().reset_index()

    def graph():
        fig = px.bar(df,
                      x="hour",
                      y="mw",
                      color='tech',
                      facet_row=y_granularity,
                      facet_col="year",
                      title= 'Res gen: ' + scenario,
                      hover_data= {'year':False,
                                   'season':False,
                                   'hour':True,
                                   'mw':True},
                      color_discrete_sequence = px.colors.qualitative.Light24)
        print('once')
        colors = ['blue', 'green', 'red']
        # Group dem_df by year and y_granularity
        for c,year in enumerate(years):
            for r,gran in enumerate(seasons):
                temp_df = dem_df[(dem_df['year'] == year) & (dem_df[y_granularity] == gran)]
                for idx, variable_value in enumerate(dem_type):
                    variable_df = temp_df[temp_df['variable'] == variable_value]
                    fig.add_trace(go.Scatter(x=variable_df['hour'], y=variable_df['value'],
                                             mode='lines',
                                             line=dict(color=colors[idx]),
                                             hoverinfo='text',
                                             text=[f'{variable_value}: {value}'
                                                   for value in variable_df['value']],
                                             showlegend=False),row=4-r,col=c+1)

        fig.update_layout(legend=dict(
            orientation="h",
            yanchor="bottom",
            xanchor="auto"),
            hovermode="x unified",
            height=1700)
        fig.write_html("out/dem_gen_bar.html")
        fig.show()

    graph()
    return


dem_all_techs_gen_area(base,tuple(years),y_granularity)
dem_all_techs_gen_bar(base,tuple(years),y_granularity)