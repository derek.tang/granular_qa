import numpy as np
import os
import pandas as pd
import psycopg2
from contextlib import closing
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objects as go
import plotly.express as px
import plotly.io as pio
import plotly.subplots as sp

pio.renderers.default = 'browser'

def sql(sqlQuery, params=None, dbname='production'):
    """ Takes a SQL query, opens a connection, queries the data, puts them into an pandas dataframe
    and closes the connection """
    user = os.environ.get('dwh_user_name')
    password = os.environ.get('dwh_password')

    host = "warehouse.chgkrthnr8sn.eu-west-2.redshift.amazonaws.com"

    user = user + "_" + dbname
    port = "5439"
    connect_string = f"host={host} dbname={dbname} user={user} password={password} port={port}"
    with closing(psycopg2.connect(connect_string)) as conn:
        out_df = pd.read_sql_query(sqlQuery, conn, params=params)
    return out_df

def time_labels(df):
    df['date'] = pd.to_datetime(df['date'])
    df.set_index('date', inplace=True)
    df['year'] = df.index.year
    df['month'] = df.index.month
    df['day'] = df.index.day
    df['hour'] = df.index.hour
    df['season'] = pd.cut(df.month, bins=[0,2,5,8,11,12], labels=['Winter', 'Spring', 'Summer', 'Fall','Winter'],ordered=False)
    return df

def caiso_wide_cap(scenario):
    query = f'''
    select
    extract(year from date) as year,
    tech,
    sum(capacity) as capacity_gw
    from (
        select
        t.date,
        CASE
            when technology  in ('nuk') then 'Nuclear'
            when technology in ('ccg') then 'Gas CCGT'
            when technology in ('gasccs') then 'Gas CCS'
            when technology in ('coa','oth') then 'Other Thermal'
            when technology in ('sor') then 'Rooftop Solar'
            when technology in ('sol','sot') then 'Utility Solar'
            when technology in ('bio','geo','ogr') then 'Other Renewables'
            when technology in ('won') then 'Onshore Wind'
            when technology in ('wof') then 'Offshore Wind'
            when technology in ('sto','ror') then 'Hydro'
            when technology in ('rge','occ','ocg','ico') then 'Peaking'
            when technology in ('bat','ips','ipc') then 'Battery Storage'
            when technology in ('pum') then 'Pumped Storage'
            else 'Other'
        END as tech,
        sum(ypo.capacity) as capacity
        from yearlyplantoperations ypo
        inner join time t on t.id = ypo.timeid
        inner join scenarios s on s.id = ypo.scenarioid
        inner join plants p on p.id = ypo.plantid
        inner join regions r on r.id = p.regionid
        inner join technologies tc on tc.id = p.technologyid
        where s.scenario = '{scenario}' and r.region in ('NP15','SP15','ZP26')--change this line to fit the timeframe
        group by t.date, tech) as sub
    group by year, tech
    order by year;
    '''
    df = sql(query).pivot_table(index='year', columns='tech', values='capacity_gw')
    return df


base = "PMF CAI 24 JAN CENTRAL FINAL-FYR"


def incremental_build(scenario):
    df = caiso_wide_cap(scenario)
    increment_deltas = df.diff().reset_index().melt(value_vars=['Battery Storage','Gas CCGT','Hydro','Nuclear','Offshore Wind','Onshore Wind','Other Renewables','Other Thermal','Peaking','Pumped Storage','Rooftop Solar','Utility Solar'],id_vars=['year'])
    fig = px.bar(increment_deltas,x='year',y='value',color='tech')
    fig.write_html("out/incremental_build.html")
    fig.show()
    return


incremental_build(base)
