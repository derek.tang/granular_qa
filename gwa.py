import numpy as np
import os
import pandas as pd
import psycopg2
from contextlib import closing
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objects as go
import plotly.express as px
import plotly.io as pio
import plotly.subplots as sp

pio.renderers.default = 'browser'

def sql(sqlQuery, params=None, dbname='production'):
    """ Takes a SQL query, opens a connection, queries the data, puts them into an pandas dataframe
    and closes the connection """
    user = os.environ.get('dwh_user_name')
    password = os.environ.get('dwh_password')

    host = "warehouse.chgkrthnr8sn.eu-west-2.redshift.amazonaws.com"

    user = user + "_" + dbname
    port = "5439"
    connect_string = f"host={host} dbname={dbname} user={user} password={password} port={port}"
    with closing(psycopg2.connect(connect_string)) as conn:
        out_df = pd.read_sql_query(sqlQuery, conn, params=params)
    return out_df

def gwas(scenario,base):
    query = f'''
    SELECT
      t1.scenario,
      t1.year,
      t1.region,
      t1.technology,
      SUM(t1.canproductioninmwh * t2.wholesaleprice)/SUM(t1.canproductioninmwh) as captureprice
    from (
      SELECT
        s.scenario,
        t.year,
        t.hourinyear,
        r.region,
        te.technology,
        SUM(hhp.canproductioninmw) as canproductioninmwh
      from halfhourlyplant hhp
      inner join time t on t.id = hhp.timeid
      inner join scenarios s on s.id = hhp.scenarioid
      inner join plants p on p.id = hhp.plantid
      inner join regions r on r.id = p.regionid
      inner join technologies te on te.id = p.technologyid
      where
        s.scenario in ('{scenario}','{base}') and
        r.region in ('NP15','SP15','ZP26') and
        te.technology in ('sol','won')
      group by 1,2,3,4,5) t1
    inner join (
      Select
        s.scenario,
        t.year,
        t.hourinyear,
        r.region,
        hhr.wholesaleprice
      from halfhourlyregion hhr
      inner join time t on t.id = hhr.timeid
      inner join scenarios s on s.id = hhr.scenarioid
      inner join regions r on r.id = hhr.regionid
      where
        s.scenario in ('{scenario}','{base}') and
        r.region in ('NP15','SP15','ZP26')
      group by 1,2,3,4,5) t2 on t1.year = t2.year and t1.hourinyear = t2.hourinyear and t1.region = t2.region  and t1.scenario = t2.scenario
    group by 1,2,3,4
    order by 1,2,3,4
    '''
    df= sql(query)#.pivot(index = ["year","month"],columns=['region','technology'], values= 'captureprice')
    df.to_csv('out/'+scenario+ '_gwa.csv')
    x = np.arange(2024,2051)
    fig = px.line(
        df,
        x='year',
        y='captureprice',
        color='region',
        line_dash='scenario',
        line_group='technology'
    )

    fig.update_layout(
        legend=dict(
            orientation="h",
            yanchor="bottom",
            xanchor="auto"),
        margin=dict(l=0, r=0, t=0, b=0),
        yaxis=dict(range=[0, None])
    )
    fig.show()
    fig.write_html('out/gwa_comp.html')
    return

gwas('PMF CAI 24 JAN CENTRAL FINAL-FYR','PMF CAI 23 OCT CENTRAL FINAL-FYR')
