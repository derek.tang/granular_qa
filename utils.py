import os
import numpy as np
import pandas as pd
import psycopg2
from contextlib import closing
import seaborn as sns
import plotly.graph_objects as go
import plotly.express as px
import plotly.io as pio
from plotly.subplots import make_subplots

pio.renderers.default = 'browser'


def sql(sqlquery, params=None, dbname='production'):
    """ Takes a SQL query, opens a connection, queries the data, puts them into an pandas dataframe
                          and closes the connection """

    user = os.environ.get('dwh_user_name')
    password = os.environ.get('dwh_password')

    host = "warehouse.chgkrthnr8sn.eu-west-2.redshift.amazonaws.com"

    user = user + "_" + dbname
    port = "5439"
    connect_string = f"host={host} dbname={dbname} user={user} password={password} port={port}"
    with closing(psycopg2.connect(connect_string)) as conn:
        out_df = pd.read_sql_query(sqlquery, conn, params=params)
    print('query done')
    return out_df