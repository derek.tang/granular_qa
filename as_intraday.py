import os
import numpy as np
import pandas as pd
import psycopg2
from contextlib import closing
import seaborn as sns
import plotly.graph_objects as go
import plotly.express as px
import plotly.io as pio
from plotly.subplots import make_subplots

pio.renderers.default = 'browser'


def sql(sqlquery, params=None, dbname='production'):
    """ Takes a SQL query, opens a connection, queries the data, puts them into an pandas dataframe
                          and closes the connection """

    user = os.environ.get('dwh_user_name')
    password = os.environ.get('dwh_password')

    host = "warehouse.chgkrthnr8sn.eu-west-2.redshift.amazonaws.com"

    user = user + "_" + dbname
    port = "5439"
    connect_string = f"host={host} dbname={dbname} user={user} password={password} port={port}"
    with closing(psycopg2.connect(connect_string)) as conn:
        out_df = pd.read_sql_query(sqlquery, conn, params=params)
    print('query done')
    return out_df


def time_labels(df):
    df['date'] = pd.to_datetime(df['date'])
    df.set_index('date', inplace=True)
    df['year'] = df.index.year
    df['month'] = df.index.month
    df['day'] = df.index.day
    df['hour'] = df.index.hour
    df['season'] = pd.cut(df.month, bins=[0, 2, 5, 8, 11, 12], labels=['Winter', 'Spring', 'Summer', 'Fall', 'Winter'],
                          ordered=False)
    return df


# INPUTS: Edit parameters here.
base = "PMF CAI 23 OCT MESSY TRANSITION FINAL-FYR"
scenario = "PMF CAI 24 JAN MT dev 16-FYR"
years = [2024,2030,2035,2043]
years_tuple = tuple(years)
y_granularity = 'season' # 'month' or 'season'


def anc(scenario,alt_scenario):
    anc_query = f"""
    Select s.scenario, t.date, r.region, anc.service, clearingpricepermwperh as price
    from halfhourlyregionancillary hhra
    inner join time t on t.id = hhra.timeid
    inner join scenarios s on s.id = hhra.scenarioid
    inner join regions r on r.id = hhra.regionid
    inner join ancillaryservices anc on anc.id = hhra.serviceid
    where s.scenario in ('{scenario}','{alt_scenario}') and 
    r.region in ('NP15','SP15','ZP26') and 
    t.year in {years_tuple}
    order by 1
    """

    df = sql(anc_query)
    df = time_labels(df)
    return df


his_np_anc=f'''
select
  key__date_time_utc at time zone 'america/los_angeles' as date,
  key__category as market,
  sum(price) price
from ad_hoc.usa_caiso_1h_dam_ancillary_service_price
where key__category in ('RU', 'RD', 'SR', 'NR')
and key__region in ('AS_CAISO_EXP', 'AS_CAISO', 'AS_NP26_EXP', 'NP26 EXP Part', 'NP26 Part', 'AS_NP26')
group by date, key__category
order by date, key__category
'''

his_sp_anc=f'''
select
  key__date_time_utc at time zone 'america/los_angeles' as date,
  key__category as market,
  sum(price) price
from ad_hoc.usa_caiso_1h_dam_ancillary_service_price
where key__category in ('RU', 'RD', 'SR', 'NR')
and key__region in ('AS_CAISO_EXP', 'AS_CAISO', 'AS_SP26_EXP', 'AS_SP26_P', 'AS_SP26', 'AS_SP15_EXP')
group by date, key__category
order by date, key__category

'''


def forecast(current,previous):
    df = anc(current,previous)
    regions = ['SP15'] #All regions have the same AS forecast

    for r in regions:
        filter_df = df[(df['year'].isin(years)) & (df['region'] == r)].groupby(['scenario','year',y_granularity,'hour','region','service'])['price'].mean().reset_index()
        fig = px.line(filter_df,
                      x="hour",
                      y="price",
                      color='service',
                      facet_row=y_granularity,
                      facet_col="year",
                      title= 'AS clearing price: ' + r,
                      line_dash = 'scenario',
                      hover_data= {'region':False,
                                   'scenario':False,
                                   y_granularity:False,
                                   'year':False,
                                   'hour':True,
                                   'price':True})

        fig.update_layout(legend=dict(
            orientation="h",
            yanchor="bottom",
            xanchor="auto"),
            hovermode="x unified",
            height=1700)

        fig.show()
        fig.write_html('out/forecasted_as.html')


def historical(his_atc,region_name):
    his= sql(his_atc).reset_index()
    his_filtered = time_labels(his)
    his_filtered =his_filtered.groupby(['year',y_granularity,'hour','market'])['price'].mean().reset_index()

    fig = px.line(his_filtered,
                  x="hour",
                  y="price",
                  color='market',
                  facet_row=y_granularity,
                  facet_col="year",
                  title= 'AS clearing price: ',
                  hover_data= {y_granularity:False,
                               'year':False,
                               'hour':True,
                               'price':True})

    fig.update_layout(legend=dict(
        orientation="h",
        yanchor="bottom",
        xanchor="auto"),
        hovermode="x unified",
        height=1700)

    fig.show()
    fig.write_html('out/'+region_name+'-his_as.html')


forecast(base,scenario)
historical(his_np_anc,'NP15')
historical(his_sp_anc,'SP15')
#%%
